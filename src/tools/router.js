/**
 * 页面跳转路由
 */
let navigator = weex.requireModule('navigator');

const routes = [
    {path:'index', component:'index',name:"首页"},
    {path:'error', component:'error',name:"错误页面"},
    {path:'modules/first-page', component:'first-page',name:"第一页"},
    {path:'modules/setter-page',component:'setter-page',name:"设置页"},
    {path:'modules/polycy-page',component:'polycy-page',name:"隐私政策"},
    {path:'modules/server-page',component:'server-page',name:"服务"},
    {path:'modules/feedback-page',component:'feedback-page',name:"反馈"}
];

function getRoute(component) {
    let name = "";
    let targets = routes.filter(function (route) {
        return route.component === component;
    });
    if(targets.length > 0){
        name = targets.pop().path;
    } else {
        name = "error";
    }
    let arr = weex.config.bundleUrl.split("/");
    arr.pop();
    if(arr.includes("modules")){
        arr.pop();
    }
    if (weex.config.env.platform === "Web"){
        arr.push(name + ".html");
        return arr.join("/");
    } else {
        arr.push(name + ".js");
        return arr.join("/");
    }
}

export default function navigateToNextPage(component) {
    navigator.push({
        url:getRoute(component),
        animated:"true"
    })
} 
// function popToPreviousPage() {
//     navigator.pop({
//         animated: "true"
//      }, event => {
//          console.log('callback: ', event)
//       })
// }
